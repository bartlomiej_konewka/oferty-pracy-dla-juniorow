package com.junioroffers.mongo.config;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.junioroffers.offer.domain.Offer;
import com.junioroffers.offer.domain.OfferRepository;

import java.util.Arrays;

@ChangeLog(order = "1")
public class DatabaseChangeLog {
    @ChangeSet(order = "001", id = "seedDatabase", author = "Bartek")
    public void seedDatabase(OfferRepository offerRepository) {
        offerRepository.insert(Arrays.asList(acaisoftOffer(), codinoOffer()));
    }

    private Offer acaisoftOffer() {
        final Offer offer = new Offer();
        offer.setId("ddc99417-1265-4b67-a31e-9b1007682d76");
        offer.setOfferUrl("https://nofluffjobs.com/pl/job/junior-java-engineer-remote-acaisoft-poland-plunxt6e");
        offer.setPosition("Junior Java Engineer (Remote)");
        offer.setSalary("3k - 7k PLN");
        offer.setCompanyName("Acaisoft Poland Sp. z o.o.");
        return offer;
    }

    private Offer codinoOffer() {
        final Offer offer = new Offer();
        offer.setId("1370e8b6-fa95-4705-b322-50940076633d");
        offer.setOfferUrl("https://nofluffjobs.com/pl/job/junior-developer-codino-warszawa-nkdvsmnx");
        offer.setPosition("(Junior) Developer");
        offer.setSalary("5k - 10k PLN");
        offer.setCompanyName("Codino");
        return offer;
    }
}

