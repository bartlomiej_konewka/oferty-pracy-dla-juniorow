package com.junioroffers.offer.domain;

public interface SampleOffer
{
    default Offer acaisoftOffer() {
        final Offer offer = new Offer();
        offer.setId("ddc99417-1265-4b67-a31e-9b1007682d76");
        offer.setOfferUrl("https://nofluffjobs.com/pl/job/junior-java-engineer-remote-acaisoft-poland-plunxt6e");
        offer.setPosition("Junior Java Engineer (Remote)");
        offer.setSalary("3k - 7k PLN");
        offer.setCompanyName("Acaisoft Poland Sp. z o.o.");
        return offer;
    }

    default Offer codinoOffer() {
        final Offer offer = new Offer();
        offer.setId("1370e8b6-fa95-4705-b322-50940076633d");
        offer.setOfferUrl("https://nofluffjobs.com/pl/job/junior-developer-codino-warszawa-nkdvsmnx");
        offer.setPosition("(Junior) Developer");
        offer.setSalary("5k - 10k PLN");
        offer.setCompanyName("Codino");
        return offer;
    }
    default Offer createSampleOffer(String id, String offerUrl, String position, String salary, String companyName){
        final Offer offer =  new Offer();
        offer.setId(id);
        offer.setId(offerUrl);
        offer.setPosition(position);
        offer.setSalary(salary);
        offer.setCompanyName(companyName);
        return offer;
    }
}
