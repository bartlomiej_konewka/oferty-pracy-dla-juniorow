package com.junioroffers.offer.domain.dto;

import com.junioroffers.offer.domain.OfferMapper;
import com.junioroffers.offer.domain.SampleOffer;

public interface SampleOfferDto extends SampleOffer
{
    default OfferDto acaisoftOfferDto() {
        return OfferMapper.mapToOfferDto(acaisoftOffer());
    }

    default OfferDto codinoOfferDto() {
        return OfferMapper.mapToOfferDto(codinoOffer());
    }
}
