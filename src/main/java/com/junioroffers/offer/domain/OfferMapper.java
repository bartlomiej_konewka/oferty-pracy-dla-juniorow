package com.junioroffers.offer.domain;

import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.offer.domain.dto.OfferDto;

public class OfferMapper {
    public static OfferDto mapToOfferDto(Offer offer) {
        return OfferDto.builder()
                .id(offer.getId())
                .companyName(offer.getCompanyName())
                .position(offer.getPosition())
                .salary(offer.getSalary())
                .offerUrl(offer.getOfferUrl())
                .build();
    }
    public static Offer mapToOffer(JobOfferDto jobOfferDto) {
        Offer offer = new Offer();
        offer.setCompanyName(jobOfferDto.getCompany());
        offer.setSalary(jobOfferDto.getSalary());
        offer.setPosition(jobOfferDto.getTitle());
        offer.setOfferUrl(jobOfferDto.getOfferUrl());
        return offer;
    }
}
