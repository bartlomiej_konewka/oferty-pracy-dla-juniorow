package com.junioroffers.offer.domain;

import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OfferService {
    private final OfferRepository offerRepository;

    public List<OfferDto> findAllOffers() {
        List<Offer> offers = offerRepository.findAll();
        return offers.stream()
                .map(OfferMapper::mapToOfferDto)
                .collect(Collectors.toList());
    }

    public OfferDto findOfferById(String id) {
        return offerRepository.findById(id)
                .map(OfferMapper::mapToOfferDto)
                .orElseThrow(() -> new OfferNotFoundException("Offer with id " + id + " not found"));
    }

    public List<Offer> saveAll(List<Offer> offers){
        offerRepository.saveAll(offers);
        return offers;
    }

    public List<Offer> saveAllJobOfferDto(List<JobOfferDto> offersDto){
        List<Offer> offerList = offersDto.stream()
                .filter(jobOfferDto -> doesOfferWithGivenUrlNotExists(jobOfferDto))
                .filter(jobOfferDto -> isOffersNotEmpty(jobOfferDto))
                .map(OfferMapper::mapToOffer)
                .collect(Collectors.toList());
        return offerRepository.saveAll(offerList);
    }

    private boolean isOffersNotEmpty(JobOfferDto jobOfferDto) {
        return !jobOfferDto.getOfferUrl().isEmpty();
    }

    private boolean doesOfferWithGivenUrlNotExists(JobOfferDto jobOfferDto) {
        return !offerRepository.existsByOfferUrl(jobOfferDto.getOfferUrl());
    }
}

