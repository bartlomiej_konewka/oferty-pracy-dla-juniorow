package com.junioroffers.offer.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class OfferControllerErrorHandler {
    @ExceptionHandler(OfferNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public OfferErrorResponse offerNotFoundException(OfferNotFoundException exception) {
        OfferErrorResponse offerErrorResponse =
                new OfferErrorResponse(exception.getMessage(),
                        HttpStatus.NOT_FOUND);
        return offerErrorResponse;
    }
}
