package com.junioroffers.offer.scheduling;

import com.junioroffers.infrastructure.RemoteOfferClient;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.offer.domain.OfferMapper;
import com.junioroffers.offer.domain.OfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class HttpOfferScheduler
{
    private final RemoteOfferClient offerClient;
    private final OfferService offerService;

    @Scheduled(fixedDelayString = "${http.offers.scheduler.request.delay}")
    void getOffersFromHttpService(){
        List<JobOfferDto> offers = offerClient.getOffers();
        offerService.saveAllJobOfferDto(offers);
    }
}
