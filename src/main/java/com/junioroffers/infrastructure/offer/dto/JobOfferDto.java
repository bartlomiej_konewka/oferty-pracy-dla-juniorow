package com.junioroffers.infrastructure.offer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobOfferDto
{
    private String title;
    private String company;
    private String salary;
    private String offerUrl;
}
