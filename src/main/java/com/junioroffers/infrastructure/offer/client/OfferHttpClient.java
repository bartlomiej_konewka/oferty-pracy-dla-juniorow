package com.junioroffers.infrastructure.offer.client;

import com.junioroffers.infrastructure.RemoteOfferClient;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public class OfferHttpClient implements RemoteOfferClient {

    private final String host;
    private final int port;
    private final RestTemplate restTemplate;

    @Override
    public List<JobOfferDto> getOffers() {
        HttpEntity httpEntity = new HttpEntity(getHeaders());
        try {
            List<JobOfferDto> jobOfferDtos = restTemplate.exchange(
                    createUrl("/offers"),
                    HttpMethod.GET,
                    httpEntity,
                    new ParameterizedTypeReference<List<JobOfferDto>>() {
                    }).getBody();

            if (jobOfferDtos == null) return Collections.emptyList();
            else {
                return jobOfferDtos;
            }
        } catch (ResourceAccessException exception) {
            return Collections.emptyList();
        }
    }

    private String createUrl(String path) {
        return host + ":" + port + path;
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}

