package com.junioroffers.config;

import com.junioroffers.infrastructure.RemoteOfferClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;



public class HttpClientConfigTest extends HttpClientConfig {

    public RemoteOfferClient remoteOfferClient(String host, int port, int connectionTimeout, int readTimeout) {
        RestTemplate restTemplate = restTemplate(connectionTimeout, readTimeout, restTemplateResponseErrorHandler());
        return remoteOfferClient(restTemplate, host, port);
    }
}
