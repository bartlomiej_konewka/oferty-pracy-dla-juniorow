package com.junioroffers.infrastructure.offer.client;

import com.junioroffers.infrastructure.RemoteOfferClient;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OfferClientTest {
    RestTemplate restTemplate = mock(RestTemplate.class);

    @Test
    public void shouldReturnedEmptyList() {
        RemoteOfferClient offerClient = new OfferHttpClient("https://programming-masterpiece.com", 1080, restTemplate);
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<JobOfferDto>>>any()
        )).thenReturn(ResponseEntity.ok(Collections.EMPTY_LIST));
        Assertions.assertThat(offerClient.getOffers()).isEqualTo(Collections.emptyList());
    }

    @Test
    public void shouldReturnedOneElementList() {
        RemoteOfferClient offerClient = new OfferHttpClient("https://programming-masterpiece.com", 1080, restTemplate);
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<JobOfferDto>>>any()
        )).thenReturn(ResponseEntity.ok(Arrays.asList(
                new JobOfferDto("name", "Junior Java Developer", "4000-5000", "link"))));
        Assertions.assertThat(offerClient.getOffers().size()).isEqualTo(1);
    }

    @Test
    public void shouldReturnedTwoElementList() {
        RemoteOfferClient offerClient = new OfferHttpClient("https://programming-masterpiece.com", 1080, restTemplate);
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<JobOfferDto>>>any()
        )).thenReturn(ResponseEntity.ok(Arrays.asList(
                new JobOfferDto("name", "Junior Java Developer", "4000-5000", "link"),
                new JobOfferDto("name2", "Java Developer", "7000-8000", "link2")
        )));
        Assertions.assertThat(offerClient.getOffers().size()).isEqualTo(2);
    }
}
