package com.junioroffers.infrastructure.offer.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.junioroffers.config.HttpClientConfig;
import com.junioroffers.config.HttpClientConfigTest;
import com.junioroffers.infrastructure.RemoteOfferClient;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.util.SocketUtils;

import java.util.Collections;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.BDDAssertions.then;

public class OfferClientIntegrationTest extends HttpClientConfig {
    int port = SocketUtils.findAvailableTcpPort();
    WireMockServer wireMockServer;
    RemoteOfferClient offerClient = new HttpClientConfigTest()
            .remoteOfferClient("http://localhost", port, 1000, 1000);

    @BeforeEach
    void setup() {
        wireMockServer = new WireMockServer(options().port(port));
        wireMockServer.start();
        WireMock.configureFor(port);
    }

    @AfterEach
    void stop() {
        wireMockServer.stop();
    }

    @Test
    public void shouldReturnOneOffer() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse().withHeader("Content-Type", "application/json")
                        .withBody(OneJsonOfferConvertedToString())));
        then(offerClient.getOffers().size()).isEqualTo(1);
    }

    @Test
    public void shouldReturnEmptyList() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse().withHeader("Content-Type", "application/json")));
        then(offerClient.getOffers()).isEqualTo(Collections.emptyList());
    }

    @Test
    public void shouldReturnEmptyListWhenEmptyResponse() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withFault(Fault.EMPTY_RESPONSE)));
        then(offerClient.getOffers()).isEqualTo(Collections.emptyList());
    }

    @Test
    public void shouldReturnMessageWhenBadRequestHasSend() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .withHeader("Content-Type", "application/json")));
        BDDAssertions.thenThrownBy(() -> offerClient.getOffers()).hasMessageContaining("500 INTERNAL_SERVER_ERROR");
    }

    @Test
    public void shouldReturnMessageWhenResourceNotFound() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())
                        .withHeader("Content-Type", "application/json")));

        BDDAssertions.thenThrownBy(() -> offerClient.getOffers()).hasMessageContaining("404 NOT_FOUND");
    }

    @Test
    public void shouldReturnMessageWhenClientUnauthorized() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.UNAUTHORIZED.value())
                        .withHeader("Content-Type", "application/json")));

        BDDAssertions.thenThrownBy(() -> offerClient.getOffers()).hasMessageContaining("401 UNAUTHORIZED");
    }

    @Test
    public void shouldReturnZeroJobOfferWhenResponseIsDelayed() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", "application/json")
                        .withBody(OneJsonOfferConvertedToString())
                        .withFixedDelay(2000)));

        then(offerClient.getOffers().size()).isEqualTo(0);
    }

    private String OneJsonOfferConvertedToString() {
        return "[{\n" +
                "   \"title\": \"Junior AIOps Software Consultant, German NOWA\",\n" +
                "   \"company\": \"AppDynamics\",\n" +
                "   \"salary\": \"7k - 9k PLN\",\n" +
                "   \"offerUrl\": \"https://nofluffjobs.com/pl/job/junior-aiops-software-consultant-german-appdynamics-krakow-igbrpbuv\"\n" +
                "}]";
    }
}

