package com.junioroffers.offer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.offer.domain.OfferRepository;
import com.junioroffers.offer.domain.OfferService;
import com.junioroffers.offer.domain.SampleOffer;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.junioroffers.offer.domain.exceptions.OfferErrorResponse;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(classes = Config.class)
class OfferControllerTest {

    @Test
    public void shouldReturnStatusOkWhenGetOffers(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/offers"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnStatusNotFoundWhenGetOffers(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/offerss"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnCorrectJson(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/offers"))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        OfferDto[] offerArray = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), OfferDto[].class);
        List<OfferDto> offerList = Arrays.asList(offerArray);
        assertThat(offerList.size()).isEqualTo(2);
    }

    @Test
    public void shouldReturnStatusOkAndCorrectJsonWhenGetOffersById(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/offers/ddc99417-1265-4b67-a31e-9b1007682d76"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        OfferDto offer = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), OfferDto.class);
        assertThat(offer.getPosition()).isEqualTo("Junior Java Engineer (Remote)");
    }

    @Test
    public void shouldThrowOfferNotFoundException(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        OfferErrorResponse offerErrorResponse = new OfferErrorResponse("Offer with id 10 not found", HttpStatus.NOT_FOUND);
        String expectedValue = objectMapper.writeValueAsString(offerErrorResponse);
        MvcResult mvcResult = mockMvc.perform(get("/offers/10"))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();
        String actualValue = mvcResult.getResponse().getContentAsString();
        assertThat(actualValue).isEqualTo(expectedValue);
    }
}


class Config implements SampleOffer, SampleOfferDto {
    @Bean
    OfferControllerErrorHandler offerControllerErrorHandler() {
        return new OfferControllerErrorHandler();
    }

    @Bean
    OfferService offerService() {
        OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
        return new OfferService(offerRepository) {
            @Override
            public List<OfferDto> findAllOffers() {
                return Arrays.asList(codinoOfferDto(), acaisoftOfferDto());
            }

            @Override
            public OfferDto findOfferById(String id) {
                if (id.equals("ddc99417-1265-4b67-a31e-9b1007682d76")) return acaisoftOfferDto();
                else if (id.equals("1370e8b6-fa95-4705-b322-50940076633d")) return codinoOfferDto();
                else throw new OfferNotFoundException("Offer with id " + id + " not found");
            }
        };
    }

    @Bean
    OfferController offerController(OfferService offerService) {
        return new OfferController(offerService);
    }


}


