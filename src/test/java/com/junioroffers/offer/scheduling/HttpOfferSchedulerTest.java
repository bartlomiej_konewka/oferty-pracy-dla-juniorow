package com.junioroffers.offer.scheduling;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.infrastructure.RemoteOfferClient;
import org.awaitility.Duration;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("scheduler")
class HttpOfferSchedulerTest
{
    @SpyBean
    RemoteOfferClient offerClient;

    @Test
    public void whenWaitOneSecond_thenScheduledIsCalledAtLeastTenTimes() {
        await()
                .atMost(Duration.TEN_SECONDS)
                .untilAsserted(() -> verify(offerClient, times(2)).getOffers());
    }
}
