package com.junioroffers.offer.domain;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.BDDAssertions.then;


@SpringBootTest(classes = JobOffersApplication.class)
@Testcontainers
@ActiveProfiles("container")
class OfferServiceWithContainerTests implements SampleOffer{

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void shouldReturnAllOffers(@Autowired OfferRepository offerRepository){
        then(offerRepository.findAll()).containsAll(Arrays.asList(codinoOffer(), acaisoftOffer()));
    }

    @Test
    public void shouldAddOnlyOneOfferIfOfferUrlAlreadyExist(@Autowired OfferRepository offerRepository,
                                                            @Autowired OfferService offerService){
        JobOfferDto existingOffer =
                new JobOfferDto("offer1", "company1", "salary1", "url1");

        JobOfferDto shouldAddOffer =
                new JobOfferDto("offer2", "company2", "salary2", "url2");

        JobOfferDto shouldNotAddOffer =
                new JobOfferDto("offer3", "company3", "salary3", "url1");

        offerRepository.save(OfferMapper.mapToOffer(existingOffer));

        List<Offer> actualAddedOffers = offerService.saveAllJobOfferDto(Arrays.asList(shouldAddOffer, shouldNotAddOffer));


        assertThat(actualAddedOffers.size()).isEqualTo(1);
        assertThat(offerRepository.findAll().size()).isEqualTo(4); //2 obiekty na starcie dodajemy do bazy danych
        assertThat(offerRepository.existsByOfferUrl("url2")).isTrue();

    }
}
