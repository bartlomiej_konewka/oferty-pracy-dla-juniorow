package com.junioroffers.offer.domain;

import com.junioroffers.JobOffersApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(classes = JobOffersApplication.class)
@Testcontainers
@ActiveProfiles("container")
class OfferServiceSaveAllWithContainerTests implements SampleOffer
{
    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void shouldAddTwoOffers(@Autowired OfferService offerService, @Autowired OfferRepository offerRepository){
        Offer sampleOffer = createSampleOffer("530ef492-3c9c-478f-9921-a01f78590bf9",
                "offerUrl",
                "position",
                "salary",
                "company");
        Offer sampleOffer2 = createSampleOffer("430ef492-3c9c-478f-9921-a01f78590bf9",
                "offerUrl2",
                "position2",
                "salary2",
                "company2");


        List<Offer> savedOffers = offerService.saveAll(Arrays.asList(sampleOffer, sampleOffer2));
        assertThat(offerRepository.findAll()).containsAnyElementsOf(savedOffers);
    }
}
