package com.junioroffers.offer.domain.exceptions;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;


class OfferControllerErrorHandlerTest {
    @Test
    public void shouldReturnErrorMessage() {
        OfferControllerErrorHandler offerControllerErrorHandler = new OfferControllerErrorHandler();

        OfferErrorResponse actualOfferErrorResponse = offerControllerErrorHandler
                .offerNotFoundException(new OfferNotFoundException("No offer with id: 10"));

        assertThat(actualOfferErrorResponse).isEqualTo(
                new OfferErrorResponse("No offer with id: 10", HttpStatus.NOT_FOUND));
    }
}
