package com.junioroffers.offer.domain;

import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class OfferServiceTest implements SampleOffer, SampleOfferDto {
    OfferRepository offerRepository = mock(OfferRepository.class);
    OfferService offerService = new OfferService(offerRepository);

    @Test
    public void shouldReturnTwoExpectedOffer() {
        when(offerRepository.findAll()).thenReturn(Arrays.asList(codinoOffer(), acaisoftOffer()));
        List<OfferDto> actualOffers = offerService.findAllOffers();
        assertThat(actualOffers).isEqualTo(Arrays.asList(codinoOfferDto(), acaisoftOfferDto()));
    }

    @Test
    public void shouldReturnOfferById() {
        when(offerRepository.findById("ddc99417-1265-4b67-a31e-9b1007682d76")).thenReturn(Optional.of(codinoOffer()));
        OfferDto actualOffer = offerService.findOfferById("ddc99417-1265-4b67-a31e-9b1007682d76");
        assertThat(actualOffer).isEqualTo(codinoOfferDto());
    }

    @Test
    public void shouldThrowOfferNotFoundExceptionWhenBadIdGiven() {
        when(offerRepository.findById("1370e8b6-fa95-4705-b322-50940076633d")).thenReturn(Optional.empty());
        assertThatThrownBy(() -> offerService.findOfferById("1370e8b6-fa95-4705-b322-50940076633d"))
                .isInstanceOfAny(OfferNotFoundException.class)
                .hasMessageContaining("Offer with id 1370e8b6-fa95-4705-b322-50940076633d not found");
    }
}

